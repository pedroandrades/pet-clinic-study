package org.springframework.samples.petclinic.vet;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(SpecialtyController.class)
public class SpecialtyControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private SpecialtyRepository specialties;

	private static final int TEST_SPECIALTY_ID = 1;

	@BeforeEach
	void setup() {
		Specialty radiology = new Specialty();
		radiology.setName("radiology");
		radiology.setMode("hospital");
		radiology.setId(1);
		given(this.specialties.findAll()).willReturn(Lists.newArrayList(radiology));
		given(this.specialties.findById(TEST_SPECIALTY_ID)).willReturn(radiology);
	}

	@Test
	void testShowSpecialtyList() throws Exception {
		mockMvc.perform(get("/specialties")).andExpect(status().isOk())
				.andExpect(model().attributeExists("specialties")).andExpect(view().name("vets/specialtyList"));
	}

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/specialties/new")).andExpect(status().isOk())
				.andExpect(model().attributeExists("specialty"))
				.andExpect(view().name("vets/createOrUpdateSpecialtyForm"));
	}

	@Test
	void testProcessCreationForm() throws Exception {
		mockMvc.perform(post("/specialties/new").param("name", "surgery").param("mode", "hospitalization"))
				.andExpect(status().is3xxRedirection());
	}

	@Test
	void testInitUpdateSpecialtyForm() throws Exception {
		mockMvc.perform(get("/specialties/{specialtyId}/edit", TEST_SPECIALTY_ID)).andExpect(status().isOk())
				.andExpect(model().attributeExists("specialty"))
				.andExpect(model().attribute("specialty", hasProperty("name", is("radiology"))))
				.andExpect(model().attribute("specialty", hasProperty("mode", is("hospital"))))
				.andExpect(view().name("vets/createOrUpdateSpecialtyForm"));
	}

	@Test
	void testProcessUpdateSpecialtyForm() throws Exception {
		mockMvc.perform(post("/specialties/{specialtyId}/edit", TEST_SPECIALTY_ID).param("name", "surgery")
				.param("mode", "hospitalization")).andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/specialties/{specialtyId}"));
	}

	@Test
	void testShowSpecialty() throws Exception {
		mockMvc.perform(get("/specialties/{specialtyId}", TEST_SPECIALTY_ID)).andExpect(status().isOk())
				.andExpect(model().attribute("specialty", hasProperty("name", is("radiology"))))
				.andExpect(model().attribute("specialty", hasProperty("mode", is("hospital"))))
				.andExpect(view().name("vets/specialtyDetails"));

	}

}
