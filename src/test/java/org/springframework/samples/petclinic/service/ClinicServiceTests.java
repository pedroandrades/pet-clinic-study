/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.owner.PetTypeRepository;
import org.springframework.samples.petclinic.product.Product;
import org.springframework.samples.petclinic.product.ProductRepository;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.SpecialtyRepository;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration test of the Service and the Repository layer.
 * <p>
 * ClinicServiceSpringDataJpaTests subclasses benefit from the following services provided
 * by the Spring TestContext Framework:
 * </p>
 * <ul>
 * <li><strong>Spring IoC container caching</strong> which spares us unnecessary set up
 * time between test execution.</li>
 * <li><strong>Dependency Injection</strong> of test fixture instances, meaning that we
 * don't need to perform application context lookups. See the use of
 * {@link Autowired @Autowired} on the <code>{@link
 * ClinicServiceTests#clinicService clinicService}</code> instance variable, which uses
 * autowiring <em>by type</em>.
 * <li><strong>Transaction management</strong>, meaning each test method is executed in
 * its own transaction, which is automatically rolled back by default. Thus, even if tests
 * insert or otherwise change database state, there is no need for a teardown or cleanup
 * script.
 * <li>An {@link org.springframework.context.ApplicationContext ApplicationContext} is
 * also inherited and can be used for explicit bean lookup if necessary.</li>
 * </ul>
 *
 * @author Ken Krebs
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Michael Isvy
 * @author Dave Syer
 */
@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
class ClinicServiceTests {

	@Autowired
	protected OwnerRepository owners;

	@Autowired
	protected PetRepository pets;

	@Autowired
	protected VisitRepository visits;

	@Autowired
	protected VetRepository vets;

	@Autowired
	protected PetTypeRepository pettypes;

	@Autowired
	protected SpecialtyRepository specialties;

	@Autowired
	protected ProductRepository products;

	/* PETTYPE REPOSITORY */

	@Test
	void shouldFindPetTypeById() {
		PetType petType = this.pettypes.findById(1);
		assertThat(petType.getName()).isEqualTo("cat");
		assertThat(petType.getCategory()).isEqualTo("domestic");

	}

	@Test
	void shouldFindAllPetTypesUsingPetTypeRepository() {
		Collection<PetType> petTypes = this.pettypes.findAll();
		assertThat(petTypes).hasSize(6);

		PetType petType1 = EntityUtils.getById(petTypes, PetType.class, 1);
		assertThat(petType1.getName()).isEqualTo("cat");
		assertThat(petType1.getCategory()).isEqualTo("domestic");
		PetType petType4 = EntityUtils.getById(petTypes, PetType.class, 4);
		assertThat(petType4.getName()).isEqualTo("snake");
		assertThat(petType4.getCategory()).isEqualTo("exotic");
	}

	@Test
	@Transactional
	void shouldInsertPetType() {
		Collection<PetType> petTypes = this.pettypes.findAll();
		int found = petTypes.size();

		PetType petType = new PetType();
		petType.setName("fox");
		petType.setCategory("exotic");
		this.pettypes.save(petType);
		assertThat(petType.getId().longValue()).isNotEqualTo(0);

		petTypes = this.pettypes.findAll();
		assertThat(petTypes.size()).isEqualTo(found + 1);
	}

	@Test
	@Transactional
	void shouldUpdatePetType() {
		PetType petType = this.pettypes.findById(1);
		String oldName = petType.getName();
		String newName = oldName + "X";

		petType.setName(newName);
		this.pettypes.save(petType);

		// retrieving new name from database
		petType = this.pettypes.findById(1);
		assertThat(petType.getName()).isEqualTo(newName);
	}

	/* SPECIALTY REPOSITORY */

	@Test
	void shouldFindSpecialtyById() {
		Specialty specialty = this.specialties.findById(1);
		assertThat(specialty.getName()).isEqualTo("radiology");
		assertThat(specialty.getMode()).isEqualTo("hospital");
	}

	@Test
	void shouldFIndAllSpecialties() {
		Collection<Specialty> specialties = this.specialties.findAll();
		assertThat(specialties).hasSize(3);

		Specialty specialty1 = EntityUtils.getById(specialties, Specialty.class, 1);
		assertThat(specialty1.getName()).isEqualTo("radiology");
		assertThat(specialty1.getMode()).isEqualTo("hospital");
		Specialty specialty3 = EntityUtils.getById(specialties, Specialty.class, 3);
		assertThat(specialty3.getName()).isEqualTo("dentistry");
		assertThat(specialty3.getMode()).isEqualTo("clinic");
	}

	@Test
	@Transactional
	void shouldInsertSpecialty() {
		Collection<Specialty> specialties = this.specialties.findAll();
		int found = specialties.size();

		Specialty specialty = new Specialty();
		specialty.setName("testName");
		specialty.setMode("testMode");
		this.specialties.save(specialty);
		assertThat(specialty.getId().longValue()).isNotEqualTo(0);

		specialties = this.specialties.findAll();
		assertThat(specialties.size()).isEqualTo(found + 1);
	}

	@Test
	@Transactional
	void shouldUpdateSpecialty() {
		Specialty specialty = this.specialties.findById(1);
		String oldName = specialty.getName();
		String newName = oldName + "X";

		specialty.setName(newName);
		this.specialties.save(specialty);

		// retrieving new name from database
		specialty = this.specialties.findById(1);
		assertThat(specialty.getName()).isEqualTo(newName);
	}

	/* PRODUCT REPOSITORY */

	@Test
	void shouldFindProductById() {
		Product product = this.products.findById(1);
		assertThat(product.getName()).isEqualTo("Toy");
		assertThat(product.getPrice()).isEqualTo(BigDecimal.valueOf(60));

	}

	@Test
	void shouldFIndAllProducts() {
		Collection<Product> products = this.products.findAll();
		assertThat(products).hasSize(4);

		Product product1 = EntityUtils.getById(products, Product.class, 1);
		assertThat(product1.getName()).isEqualTo("Toy");
		assertThat(product1.getPrice()).isEqualTo("60");
		Product product4 = EntityUtils.getById(products, Product.class, 4);
		assertThat(product4.getName()).isEqualTo("Accessory");
		assertThat(product4.getPrice()).isEqualTo("30");
	}

	@Test
	@Transactional
	void shouldInsertProduct() {
		Collection<Product> products = this.products.findAll();
		int found = products.size();

		Product product = new Product();
		product.setName("testName");
		product.setPrice(BigDecimal.valueOf(999));
		this.products.save(product);
		assertThat(product.getId().longValue()).isNotEqualTo(0);

		products = this.products.findAll();
		assertThat(products.size()).isEqualTo(found + 1);
	}

	@Test
	@Transactional
	void shouldUpdateProduct() {
		Product product = this.products.findById(1);
		String oldName = product.getName();
		String newName = oldName + "X";

		product.setName(newName);
		this.products.save(product);

		// retrieving new name from database
		product = this.products.findById(1);
		assertThat(product.getName()).isEqualTo(newName);
	}

	@Test
	void shouldFindOwnersByLastName() {
		Collection<Owner> owners = this.owners.findByLastName("Davis");
		assertThat(owners).hasSize(2);

		owners = this.owners.findByLastName("Daviss");
		assertThat(owners).isEmpty();
	}

	@Test
	void shouldFindSingleOwnerWithPet() {
		Owner owner = this.owners.findById(1);
		assertThat(owner.getLastName()).startsWith("Franklin");
		assertThat(owner.getPets()).hasSize(1);
		assertThat(owner.getPets().get(0).getType()).isNotNull();
		assertThat(owner.getPets().get(0).getType().getName()).isEqualTo("cat");
	}

	@Test
	@Transactional
	void shouldInsertOwner() {
		Collection<Owner> owners = this.owners.findByLastName("Schultz");
		int found = owners.size();

		Owner owner = new Owner();
		owner.setFirstName("Sam");
		owner.setLastName("Schultz");
		owner.setAddress("4, Evans Street");
		owner.setCity("Wollongong");
		owner.setTelephone("4444444444");
		this.owners.save(owner);
		assertThat(owner.getId().longValue()).isNotEqualTo(0);

		owners = this.owners.findByLastName("Schultz");
		assertThat(owners.size()).isEqualTo(found + 1);
	}

	@Test
	@Transactional
	void shouldUpdateOwner() {
		Owner owner = this.owners.findById(1);
		String oldLastName = owner.getLastName();
		String newLastName = oldLastName + "X";

		owner.setLastName(newLastName);
		this.owners.save(owner);

		// retrieving new name from database
		owner = this.owners.findById(1);
		assertThat(owner.getLastName()).isEqualTo(newLastName);
	}

	@Test
	void shouldFindPetWithCorrectId() {
		Pet pet7 = this.pets.findById(7);
		assertThat(pet7.getName()).startsWith("Samantha");
		assertThat(pet7.getOwner().getFirstName()).isEqualTo("Jean");

	}

	@Test
	void shouldFindAllPetTypes() {
		Collection<PetType> petTypes = this.pets.findPetTypes();

		PetType petType1 = EntityUtils.getById(petTypes, PetType.class, 1);
		assertThat(petType1.getName()).isEqualTo("cat");
		PetType petType4 = EntityUtils.getById(petTypes, PetType.class, 4);
		assertThat(petType4.getName()).isEqualTo("snake");
	}

	@Test
	@Transactional
	void shouldInsertPetIntoDatabaseAndGenerateId() {
		Owner owner6 = this.owners.findById(6);
		int found = owner6.getPets().size();

		Pet pet = new Pet();
		pet.setName("bowser");
		Collection<PetType> types = this.pets.findPetTypes();
		pet.setType(EntityUtils.getById(types, PetType.class, 2));
		pet.setBirthDate(LocalDate.now());
		owner6.addPet(pet);
		assertThat(owner6.getPets().size()).isEqualTo(found + 1);

		this.pets.save(pet);
		this.owners.save(owner6);

		owner6 = this.owners.findById(6);
		assertThat(owner6.getPets().size()).isEqualTo(found + 1);
		// checks that id has been generated
		assertThat(pet.getId()).isNotNull();
	}

	@Test
	@Transactional
	void shouldUpdatePetName() throws Exception {
		Pet pet7 = this.pets.findById(7);
		String oldName = pet7.getName();

		String newName = oldName + "X";
		pet7.setName(newName);
		this.pets.save(pet7);

		pet7 = this.pets.findById(7);
		assertThat(pet7.getName()).isEqualTo(newName);
	}

	@Test
	void shouldFindVets() {
		Collection<Vet> vets = this.vets.findAll();

		Vet vet = EntityUtils.getById(vets, Vet.class, 3);
		assertThat(vet.getLastName()).isEqualTo("Douglas");
		assertThat(vet.getNrOfSpecialties()).isEqualTo(2);
		assertThat(vet.getSpecialties().get(0).getName()).isEqualTo("dentistry");
		assertThat(vet.getSpecialties().get(1).getName()).isEqualTo("surgery");
	}

	@Test
	@Transactional
	void shouldAddNewVisitForPet() {
		Pet pet7 = this.pets.findById(7);
		int found = pet7.getVisits().size();
		Visit visit = new Visit();
		pet7.addVisit(visit);
		visit.setDescription("test");
		this.visits.save(visit);
		this.pets.save(pet7);

		pet7 = this.pets.findById(7);
		assertThat(pet7.getVisits().size()).isEqualTo(found + 1);
		assertThat(visit.getId()).isNotNull();
	}

	@Test
	void shouldFindVisitsByPetId() throws Exception {
		Collection<Visit> visits = this.visits.findByPetId(7);
		assertThat(visits).hasSize(2);
		Visit[] visitArr = visits.toArray(new Visit[visits.size()]);
		assertThat(visitArr[0].getDate()).isNotNull();
		assertThat(visitArr[0].getPetId()).isEqualTo(7);
	}

}
