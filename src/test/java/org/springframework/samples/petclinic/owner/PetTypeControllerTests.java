package org.springframework.samples.petclinic.owner;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(PetTypeController.class)
public class PetTypeControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PetTypeRepository petTypes;

	private static final int TEST_PETTYPE_ID = 1;

	@BeforeEach
	void setup() {
		PetType cat = new PetType();
		cat.setName("cat");
		cat.setCategory("domestic");
		cat.setId(1);
		given(this.petTypes.findAll()).willReturn(Lists.newArrayList(cat));
		given(this.petTypes.findById(TEST_PETTYPE_ID)).willReturn(cat);
	}

	@Test
	void testShowPetTypeList() throws Exception {
		mockMvc.perform(get("/pettypes")).andExpect(status().isOk()).andExpect(model().attributeExists("petTypes"))
				.andExpect(view().name("pets/petTypeList"));
	}

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/pettypes/new")).andExpect(status().isOk()).andExpect(model().attributeExists("petType"))
				.andExpect(view().name("pets/createOrUpdatePetTypeForm"));
	}

	@Test
	void testProcessCreationForm() throws Exception {
		mockMvc.perform(post("/pettypes/new").param("name", "dog").param("category", "domestic"))
				.andExpect(status().is3xxRedirection());
	}

	@Test
	void testInitUpdatePetTypeForm() throws Exception {
		mockMvc.perform(get("/pettypes/{pettypeId}/edit", TEST_PETTYPE_ID)).andExpect(status().isOk())
				.andExpect(model().attributeExists("petType"))
				.andExpect(model().attribute("petType", hasProperty("name", is("cat"))))
				.andExpect(model().attribute("petType", hasProperty("category", is("domestic"))))
				.andExpect(view().name("pets/createOrUpdatePetTypeForm"));
	}

	@Test
	void testProcessUpdatePetTypeForm() throws Exception {
		mockMvc.perform(
				post("/pettypes/{pettypeId}/edit", TEST_PETTYPE_ID).param("name", "snake").param("category", "exotic"))
				.andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/pettypes/{pettypeId}"));
	}

	@Test
	void testShowPetType() throws Exception {
		mockMvc.perform(get("/pettypes/{pettypeId}", TEST_PETTYPE_ID)).andExpect(status().isOk())
				.andExpect(model().attribute("pettype", hasProperty("name", is("cat"))))
				.andExpect(model().attribute("pettype", hasProperty("category", is("domestic"))))
				.andExpect(view().name("pets/petTypeDetails"));

	}

}
