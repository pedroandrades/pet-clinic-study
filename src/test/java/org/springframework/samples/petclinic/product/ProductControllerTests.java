package org.springframework.samples.petclinic.product;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.math.BigDecimal;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(ProductController.class)
public class ProductControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductRepository products;

	private static final int TEST_PRODUCT_ID = 1;

	private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "products/createOrUpdateProductForm";

	@BeforeEach
	void setup() {
		Product toy = new Product();
		toy.setName("Toy");
		toy.setPrice(BigDecimal.valueOf(60));
		toy.setId(1);
		given(this.products.findAll()).willReturn(Lists.newArrayList(toy));
		given(this.products.findById(TEST_PRODUCT_ID)).willReturn(toy);
	}

	@Test
	void testShowProductList() throws Exception {
		mockMvc.perform(get("/products")).andExpect(status().isOk()).andExpect(model().attributeExists("products"))
				.andExpect(view().name("products/productList"));
	}

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/products/new")).andExpect(status().isOk()).andExpect(model().attributeExists("product"))
				.andExpect(view().name(VIEWS_OWNER_CREATE_OR_UPDATE_FORM));
	}

	@Test
	void testProcessCreationForm() throws Exception {
		mockMvc.perform(post("/products/new").param("name", "accessory").param("price", "888"))
				.andExpect(status().is3xxRedirection());
	}

	@Test
	void testInitUpdateProductForm() throws Exception {
		mockMvc.perform(get("/products/{productId}/edit", TEST_PRODUCT_ID)).andExpect(status().isOk())
				.andExpect(model().attributeExists("product"))
				.andExpect(model().attribute("product", hasProperty("name", is("Toy"))))
				.andExpect(model().attribute("product", hasProperty("price", is(BigDecimal.valueOf(60)))))
				.andExpect(view().name(VIEWS_OWNER_CREATE_OR_UPDATE_FORM));
	}

	@Test
	void testProcessUpdateSpecialtyForm() throws Exception {
		mockMvc.perform(
				post("/products/{productId}/edit", TEST_PRODUCT_ID).param("name", "accessory").param("price", "888"))
				.andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/products/{productId}"));
	}

	@Test
	void testShowSpecialty() throws Exception {
		mockMvc.perform(get("/products/{productId}", TEST_PRODUCT_ID)).andExpect(status().isOk())
				.andExpect(model().attribute("product", hasProperty("name", is("Toy"))))
				.andExpect(model().attribute("product", hasProperty("price", is(BigDecimal.valueOf(60)))))
				.andExpect(view().name("products/productDetails"));

	}

}
