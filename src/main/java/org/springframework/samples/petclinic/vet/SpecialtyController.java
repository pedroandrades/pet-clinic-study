package org.springframework.samples.petclinic.vet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SpecialtyController {

	private final SpecialtyRepository specialties;

	private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "vets/createOrUpdateSpecialtyForm";

	public SpecialtyController(SpecialtyRepository specialties) {
		this.specialties = specialties;
	}

	@ModelAttribute("modes")
	public Collection<String> populateModes() {
		ArrayList<String> modes = new ArrayList<>();
		modes.add("hospital");
		modes.add("hospitalization");
		modes.add("clinic");
		return modes;
	}

	@GetMapping("/specialties")
	public String showSpecialtyList(Model model) {
		Collection<Specialty> results = this.specialties.findAll();
		model.addAttribute("specialties", results);
		return "vets/specialtyList";
	}

	@GetMapping("/specialties/new")
	public String initCreationForm(Map<String, Object> model) {
		Specialty specialty = new Specialty();
		model.put("specialty", specialty);
		return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/specialties/new")
	public String processCreationForm(Specialty specialty, BindingResult result) {
		this.specialties.save(specialty);
		return "redirect:/specialties/" + specialty.getId();

	}

	@GetMapping("/specialties/{specialtyId}/edit")
	public String initUpdateSpecialtyForm(@PathVariable("specialtyId") int specialtyId, Model model) {
		Specialty specialty = this.specialties.findById(specialtyId);
		model.addAttribute(specialty);
		return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/specialties/{specialtyId}/edit")
	public String processUpdateSpecialtyForm(Specialty specialty, BindingResult result,
			@PathVariable("specialtyId") int specialtyId) {
		specialty.setId(specialtyId);
		this.specialties.save(specialty);
		return "redirect:/specialties/{specialtyId}";

	}

	@GetMapping("/specialties/{specialtyId}")
	public String showSpecialty(@PathVariable("specialtyId") int specialtyId, Map<String, Object> model) {
		model.put("specialty", this.specialties.findById(specialtyId));
		return "vets/specialtyDetails";
	}

}
