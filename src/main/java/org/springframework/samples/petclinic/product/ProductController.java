package org.springframework.samples.petclinic.product;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ProductController {

	@Autowired
	private ProductRepository products;

	private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "products/createOrUpdateProductForm";

	@GetMapping("/products")
	public String showProductList(Model model) {
		Collection<Product> results = this.products.findAll();
		model.addAttribute("products", results);
		return "products/productList";
	}

	@GetMapping("/products/new")
	public String initCreationForm(Map<String, Object> model) {
		Product product = new Product();
		model.put("product", product);
		return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/products/new")
	public String processCreationForm(Product product, BindingResult result) {
		this.products.save(product);
		return "redirect:/products/" + product.getId();

	}

	@GetMapping("/products/{productId}/edit")
	public String initUpdateProductForm(@PathVariable("productId") int productId, Model model) {
		Product product = this.products.findById(productId);
		model.addAttribute(product);
		return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/products/{productId}/edit")
	public String processUpdateProductForm(Product product, BindingResult result,
			@PathVariable("productId") int productId) {
		product.setId(productId);
		this.products.save(product);
		return "redirect:/products/{productId}";

	}

	@GetMapping("/products/{productId}")
	public String showProduct(@PathVariable("productId") int productId, Map<String, Object> model) {
		model.put("product", this.products.findById(productId));
		return "products/productDetails";
	}

}
