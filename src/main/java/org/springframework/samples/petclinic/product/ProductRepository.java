package org.springframework.samples.petclinic.product;

import java.util.List;

import org.springframework.data.repository.Repository;

public interface ProductRepository extends Repository<Product, Integer> {

	Product findById(Integer id);

	List<Product> findAll();

	void save(Product product);

}
