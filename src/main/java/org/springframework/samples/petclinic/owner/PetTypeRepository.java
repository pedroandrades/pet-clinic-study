package org.springframework.samples.petclinic.owner;

import java.util.List;

import org.springframework.data.repository.Repository;

public interface PetTypeRepository extends Repository<PetType, Integer> {

	PetType findById(Integer id);

	List<PetType> findAll();

	void save(PetType petType);

}
