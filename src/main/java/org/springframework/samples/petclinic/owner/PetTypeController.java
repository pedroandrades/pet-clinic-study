package org.springframework.samples.petclinic.owner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PetTypeController {

	@Autowired
	private PetTypeRepository petTypes;

	private static final String VIEWS_OWNER_CREATE_OR_UPDATE_FORM = "pets/createOrUpdatePetTypeForm";

	@ModelAttribute("categories")
	public Collection<String> populateModes() {
		ArrayList<String> categories = new ArrayList<>();
		categories.add("domestic");
		categories.add("exotic");
		return categories;
	}

	@GetMapping("/pettypes")
	public String showPetTypeList(Model model) {
		Collection<PetType> results = this.petTypes.findAll();
		model.addAttribute("petTypes", results);
		return "pets/petTypeList";
	}

	@GetMapping("/pettypes/new")
	public String initCreationForm(Map<String, Object> model) {
		PetType petType = new PetType();
		model.put("petType", petType);
		return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/pettypes/new")
	public String processCreationForm(PetType petType) {
		this.petTypes.save(petType);
		return "redirect:/pettypes/" + petType.getId();

	}

	@GetMapping("/pettypes/{pettypeId}/edit")
	public String initUpdatePetTypeForm(@PathVariable("pettypeId") int pettypeId, Model model) {
		PetType petType = this.petTypes.findById(pettypeId);
		model.addAttribute(petType);
		return VIEWS_OWNER_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/pettypes/{pettypeId}/edit")
	public String processUpdatePetTypeForm(PetType petType, @PathVariable("pettypeId") int pettypeId) {
		petType.setId(pettypeId);
		this.petTypes.save(petType);
		return "redirect:/pettypes/{pettypeId}";

	}

	@GetMapping("/pettypes/{pettypeId}")
	public String showPetType(@PathVariable("pettypeId") int pettypeId, Map<String, Object> model) {
		model.put("pettype", this.petTypes.findById(pettypeId));
		return "pets/petTypeDetails";
	}

}
